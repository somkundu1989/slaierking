<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

	#Route::get('/', 'HomeController@index')->name('home');

    #Candidate Route Set
    Route::get('/', ['as' => 'home', 'uses' => 'CandidateController@index']);
    Route::get('candidate/{candidate_id}', ['as' => 'manageCandidate.single', 'uses' => 'CandidateController@single']);
    Route::post('save-candidate', ['as' => 'manageCandidate.post', 'uses' => 'CandidateController@save']);
    Route::get('delete-candidate/{candidate_id}', ['as' => 'manageCandidate.deleteCandidate', 'uses' => 'CandidateController@delete']);

    #Voter Route Set
    Route::post('voter-export-post', ['as' => 'voter-export.post', 'uses' => 'VoterController@export']);
    Route::get('/voter-export', ['as' => 'voter-export', 'uses' => 'VoterController@exportForm']);
    Route::get('/voter-import', ['as' => 'voter-import', 'uses' => 'VoterController@importForm']);
    Route::post('voter-import-post', ['as' => 'voter-import.post', 'uses' => 'VoterController@import']);