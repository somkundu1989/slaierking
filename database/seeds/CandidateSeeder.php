<?php

use App\Candidate;
use App\Constituency;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class CandidateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 15) as $index) {
            $constituency_id = Constituency::select('id')->inRandomOrder()->limit(1)->get()->first()->id;
            Candidate::create([
                'name'            => $faker->name,
                'constituency_id' => $constituency_id,
                'username'        => $faker->unique()->userName,
                'photo'           => $faker->image('storage/app/public/candidates/photo', 400, 300, 'people', false),
                'logo'            => $faker->image('storage/app/public/candidates/logo', 400, 300, 'people', false),
                'mobile_no'       => $faker->unique()->numberBetween($min = 1111111111, $max = 9999999999),
                'email'           => $faker->email,
                'password'        => bcrypt('123456'),
            ]);
        }
    }
}
