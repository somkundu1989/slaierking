<?php

use App\Constituency;
use App\Constituencytype;
use App\State;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ConstituencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 50) as $index) {
            $state_id             = State::select('id')->inRandomOrder()->limit(1)->get()->first()->id;
            $constituency_type_id = Constituencytype::select('id')->inRandomOrder()->limit(1)->get()->first()->id;
            Constituency::create([
                'name'                 => $faker->name,
                'constituency_type_id' => $constituency_type_id,
                'state_id'             => $state_id,
            ]);
        }
    }
}
