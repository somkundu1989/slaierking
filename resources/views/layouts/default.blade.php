<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>KingmakerIndia Admin</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- plugins:css -->
  <link rel="stylesheet" href="{!! asset('asset/vendors/iconfonts/mdi/css/materialdesignicons.min.css') !!}">
  <link rel="stylesheet" href="{!! asset('asset/vendors/css/vendor.bundle.base.css') !!}">
  <!-- endinject -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{!! asset('asset/css/style.css') !!}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{!! asset('asset/images/favicon.png') !!}" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="{!! route('home') !!}"><img src="{!! asset('asset/images/logo.png') !!}" style="width: 77%;height: 100%;" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="{!! route('home') !!}"><img src="{!! asset('asset/images/logo-mini.svg') !!}" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <div class="search-field d-none d-md-block">
          <form class="d-flex align-items-center h-100" action="#">
            <div class="input-group">
              <div class="input-group-prepend bg-transparent">
                  <i class="input-group-text border-0 mdi mdi-magnify"></i>                
              </div>
              <input type="text" class="form-control bg-transparent border-0" placeholder="Search projects">
            </div>
          </form>
        </div>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item nav-profile dropdown">
            <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <div class="nav-profile-img">
                <img src="{!! asset('asset/images/faces/face1.jpg') !!}" alt="image">
                <span class="availability-status online"></span>             
              </div>
              <div class="nav-profile-text">
                <p class="mb-1 text-black">David Greymaax</p>
              </div>
            </a>
            <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
              <a class="dropdown-item" href="#">
                <i class="mdi mdi-cached mr-2 text-success"></i>
                Sync Data
              </a>
              <div class="dropdown-divider"></div>
              {{-- <a class="dropdown-item" href="index.html">
                <i class="mdi mdi-logout mr-2 text-primary"></i>
                Signout
              </a> --}}
              <a class="dropdown-item" href="{{ route('logout') }}"
                 onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                 <i class="mdi mdi-logout mr-2 text-primary"></i>
                  {{ __('Logout') }}
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="{!! route('home') !!}">
              <span class="menu-title">Dashboard</span>
              <!-- <i class="mdi mdi-home menu-icon"></i> -->
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{!! route('manageCandidate.single',0) !!}">
              <span class="menu-title">Add Candidate</span>
              <!-- <i class="mdi mdi-home menu-icon"></i> -->
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{!! route('voter-export') !!}">
              <span class="menu-title">Download Voter</span>
              <!-- <i class="mdi mdi-home menu-icon"></i> -->
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{!! route('voter-import') !!}">
              <span class="menu-title">Upload Voter</span>
              <!-- <i class="mdi mdi-home menu-icon"></i> -->
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{!! asset('asset/form-4.html') !!}">
              <span class="menu-title">Mass Communication</span>
              <!-- <i class="mdi mdi-home menu-icon"></i> -->
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{!! asset('asset/form-5.html') !!}">
              <span class="menu-title">View & Edit Voter</span>
              <!-- <i class="mdi mdi-home menu-icon"></i> -->
            </a>
          </li>
        </ul>
      </nav>
      <!-- partial -->


      @yield('content')
    
        

      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{!! asset('asset/vendors/js/vendor.bundle.base.js') !!}"></script>
  <script src="{!! asset('asset/vendors/js/vendor.bundle.addons.js') !!}"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="{!! asset('asset/js/off-canvas.js') !!}"></script>
  <script src="{!! asset('asset/js/misc.js') !!}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{!! asset('asset/js/file-upload.js') !!}"></script>
  <!-- End custom js for this page-->
</body>

</html>
