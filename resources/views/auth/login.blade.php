@extends('layouts.auth')

@section('content')
  <form class="pt-3" method="POST" action="{{ route('login') }}">
      @csrf
      <div class="form-group">
          <input id="email" type="email" class="form-control form-control-lg {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

          @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
          @endif
        {{-- <input type="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Username"> --}}
      </div>
      <div class="form-group">
          <input id="password" type="password" class="form-control form-control-lg {{ $errors->has('password') ? 'is-invalid' : '' }}" name="password" required>

          @if ($errors->has('password'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
          @endif
        {{-- <input type="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password"> --}}
      </div>
      <div class="mt-3">
        {{-- <a class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" href="form-1.html">SIGN IN</a> --}}
        <button type="submit" class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn">
            {{ __('Login') }}
        </button>
      </div>
      <div class="my-2 d-flex justify-content-between align-items-center">
        <div class="form-check">
          <label class="form-check-label text-muted">
            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            Keep me signed in
          </label>
        </div>
        {{-- <a href="#" class="auth-link text-black">Forgot password?</a> --}}
      </div>
  </form>

@endsection
