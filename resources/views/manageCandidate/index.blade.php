@extends('layouts.default')

@section('content')
<div class="main-panel">
        <div class="content-wrapper">
          <!-- <div class="page-header">
            <h3 class="page-title">
              Forms
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>

              </ol>
            </nav>
          </div> -->
          <div class="row">

            <div class="col-12 grid-margin">
<style type="text/css">
.profile-card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  max-width: 300px;
  margin: auto;
  text-align: center;
  background-color: #fff;
  padding-top: 20px;
  padding-bottom: 20px;
  margin-top: 20px;
  padding-bottom: 20px;
}

.title {
  color: grey;
  font-size: 18px;
}

button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}

a {
  text-decoration: none;
  font-size: 22px;
  color: black;
}

button:hover, a:hover {
  opacity: 0.7;
}
.carousel-item{
  background-color: #fff;
  padding: 20px;
}
.carousel-control-prev-icon {
    background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23000' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E");
}

.carousel-control-next-icon {
    background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23000' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E");
}
.carousel-indicators li {
    height: 5px;
    background-color: #AAB7B8;
    box-shadow: inset 1px 1px 1px 1px rgba(0,0,0,0.5);    
}
.carousel-indicators .active {
    background-color: #000;
}
</style>
<div id="demo" class="carousel slide" data-ride="carousel" data-interval="false">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    @foreach($data['allCandidate'] as $index =>$candidateSet)
      <li data-target="#demo" data-slide-to="{!! $index !!}" class=" {!! ($index==0)?'active':'' !!}"></li>
    @endforeach
  </ul>

  <!-- The slideshow -->
  <div class="carousel-inner">
    @foreach($data['allCandidate'] as $index =>$candidateSet)
      <div class="carousel-item {!! ($index==0)?'active':'' !!}">
        <div class="row">
          @foreach($candidateSet as $key=>$candidate)
          <div class="col-lg-4">
            <div class="profile-card">
              <a href="{!! route('manageCandidate.single',$candidate->id) !!}">
              <img src="{!! (!empty($candidate->photo))?asset('storage-asset/candidates/photo/'.$candidate->photo):'' !!}" alt="{!! $candidate->name !!}" style="width:40%"></a>
              <p class="title"><a href="{!! route('manageCandidate.single',$candidate->id) !!}">{!! $candidate->name !!}</a></p>
              <p><span>1,800</span> Mobile No Added</p>
              <p><span>1,800</span> Mobile No Added</p>
              <p><span>1,800</span> Mobile No Added</p>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    @endforeach
  </div>

  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>

</div>

            </div>
          </div>
        </div>
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap Dash</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
          </div>
        </footer>
        <!-- partial -->
      </div>

@endsection
