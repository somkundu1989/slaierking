@extends('layouts.default')

@section('content')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
             
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{!! route('home') !!}">Candidate</a></li>
                <li class="breadcrumb-item active" aria-current="page">{!! (!empty($data['candidate'])) ? 'Modify' : 'Add' !!} Candidate</li>
              </ol>
            </nav>
          </div>
          <div class="row">

            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">{!! (!empty($data['candidate'])) ? 'Modify' : 'Add' !!} Candidate</h4>
                  <form class="form-sample" method="POST" action="{{ route('manageCandidate.post') }}"  enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="record_id" value="{!! (!empty($data['candidate'])) ? $data['candidate']->id : '' !!}" />
                    @if ($errors->any())
                      <div class="alert alert-danger">
                        <ul>
                          @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                        </ul>
                      </div>
                    @endif
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-5 col-form-label">Candidate Name</label>
                          <div class="col-sm-7">
                            <input type="text" class="form-control" name="name" value="{!! (!empty($data['candidate'])) ? $data['candidate']->name : '' !!}" required/>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-5 col-form-label">Constituency</label>
                          <div class="col-sm-7">
                            <select class="form-control" name="constituency_id" required>
                              @foreach($data['allConstituency'] as $index=>$value)
                                <option value="{!! $index !!}" {!! ((!empty($data['candidate'])) ? ($index == $data['candidate']->constituency_id ? 'selected="selected"' : '') : "") !!}>{!! $value !!}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-5 col-form-label">User Name</label>
                          <div class="col-sm-7">
                            <input type="text" class="form-control" name="username" value="{!! (!empty($data['candidate'])) ? $data['candidate']->username : '' !!}" required/>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-5 col-form-label">Mobile No</label>
                          <div class="col-sm-7">
                            <input type="number" class="form-control" name="mobile_no" min="1111111111" max="9999999999" value="{!! (!empty($data['candidate'])) ? $data['candidate']->mobile_no : '' !!}" required/>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-5 col-form-label">Email</label>
                          <div class="col-sm-7">
                            <input type="email" class="form-control" name="email" value="{!! (!empty($data['candidate'])) ? $data['candidate']->email : '' !!}" required/>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-5 col-form-label">Password</label>
                          <div class="col-sm-7">
                            <input type="password" class="form-control" name="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Must have at least 6 characters' : ''); if(this.checkValidity()) form.confirm_password.pattern = this.value;" placeholder="Password" required/>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-5 col-form-label">Confirm Password</label>
                          <div class="col-sm-7">
                            <input type="password" class="form-control" name="confirm_password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above' : '');" required/>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-5 col-form-label">Photo</label>
                          <div class="col-sm-7">
                            <input type="file" class="form-control" name="photo"/>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <img src="{!! (!empty($data['candidate']->photo))?asset('storage-asset/candidates/photo/'.$data['candidate']->photo):'' !!}" style="max-height: 80px;">
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-5 col-form-label">Logo</label>
                          <div class="col-sm-7">
                            <input type="file" class="form-control" name="logo"/>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <img src="{!! (!empty($data['candidate']->logo))?asset('storage-asset/candidates/logo/'.$data['candidate']->logo):'' !!}" style="max-height: 80px;">
                      </div>
                    </div>
                    <p></p>
                    <div class="row justify-content-center align-items-center">
                        <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap Dash</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
          </div>
        </footer>
        <!-- partial -->
      </div>
@endsection
