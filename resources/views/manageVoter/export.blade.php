@extends('layouts.default')

@section('content')
      <div class="main-panel">        
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
              
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Forms</a></li>
                <li class="breadcrumb-item active" aria-current="page">Download Excel</li>
              </ol>
            </nav>
          </div>
          <div class="row">
            
            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">DOWNLOAD EXCEL</h4>
                  <form class="form-sample" method="POST" action="{{ route('voter-export.post') }}"  enctype="multipart/form-data">
                    @csrf
                    <p></p>
                      <div class="row justify-content-center align-items-center">
                        <div class="form-group row">
                          <div class="col-md-12">
                            <div class="form-group row">
                              <label class="col-sm-5 col-form-label">Candidate Name</label>
                              <div class="col-sm-7">
                                <select class="form-control" name="candidate_id" onchange="((this.value!=0 )? (document.getElementById('constituency_id').value = this.value) : (document.getElementById('constituency_id').value = ''))">
                                  <option value="0">Master</option>
                                  @foreach($data['allCandidate'] as $candidate)
                                    <option value="{!! $candidate->constituency_id !!}">{!! $candidate->name !!}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-md-6" >
                            <div class="form-group row">
                              <label class="col-sm-5 col-form-label">Choose Constituency</label>
                              <div class="col-sm-7">
                                <select class="form-control" name="constituency_id" id="constituency_id" required>
                                  @foreach($data['allConstituency'] as $index=>$value)
                                    <option value="{!! $index !!}">{!! $value !!}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group row">
                              <label class="col-sm-5 col-form-label">Constituency Value</label>
                              <div class="col-sm-7">
                                <input type="text" class="form-control" name="constituency_value" required/>
                                  
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-5 col-form-label"></label>
                          
                          <div class="col-sm-7">
                            <button type="submit" class="btn btn-gradient-primary mr-2">Download</button>
                          </div>
                        </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap Dash</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
          </div>
        </footer>
        <!-- partial -->
      </div>
@endsection
