<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Constituencytype extends Model
{
    use SoftDeletes;

    protected $table = 'constituency_type_master';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id', 'name', 'created_at', 'updated_at'
    ];

    public function getAllConstituency()
    {
        return $this->hasMany('App\Constituency', 'constituency_type_id');
    }
}
