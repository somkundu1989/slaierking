<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Candidate extends Model
{
    use SoftDeletes;

    protected $table = 'candidate_master';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id', 'name', 'constituency_id', 'username', 'photo', 'mobile_no', 'email', 'password', 'logo', 'created_at', 'updated_at'
    ];

    public function constituency()
    {
        return $this->belongsTo('App\Constituency', 'constituency_id');
    }
}
