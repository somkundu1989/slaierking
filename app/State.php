<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'state_master';

    protected $fillable = [
        'id', 'name', 'created_at', 'updated_at'
    ];
    
    public function state()
    {
        return $this->hasMany('App\Constituency', 'state_id');
    }
}
