<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Votercondidateupdate extends Model
{
    use SoftDeletes;

    protected $table = 'voter_candidate_update';

    protected $dates = ['deleted_at'];

    protected $fillable = [
    'candidate_id', 'voter_id', 'voter_card_no', 'constituency_type_id', 'constituency_value', 'address', 'village_name', 'booth_address', 'society', 'bibhag', 'old_town', 'caste', 'support', 'blood_group', 'education', 'occupation', 'birth_date', 'marriage_date', 'email', 'aadhar_card', 'ration_card', 'is_dead', 'gender', 'created_at', 'updated_at'
    ];

    public function getMasterVoter()
    {
        return $this->belongsTo('App\Voter', 'voter_id');
    }
}
