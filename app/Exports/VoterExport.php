<?php

namespace App\Exports;

use App\Voter;
use App\Votercondidateupdate;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Session;

class VoterExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

        $constituency_value=Session::get('constituency_value');
        $constituency_id=Session::get('constituency_id');
        $candidate_id=Session::get('candidate_id');
        $resultArr=[];
        if($candidate_id==0){
            $allVoter = Voter::select('voter_card_no', 'constituency_type_id', 'constituency_value', 'sr_no', 'photo', 'voter_name', 'mobile_no', 'relation_name', 'surname', 'relation_type', 'house_no', 'epic_id', 'address', 'village_name', 'booth_address', 'society', 'bibhag', 'old_town', 'caste', 'support', 'blood_group', 'education', 'occupation', 'birth_date', 'marriage_date', 'email', 'aadhar_card', 'ration_card', 'is_dead', 'gender', 'is_head_of_family')->get();
            foreach ($allVoter as $key => $voter) {
            	$resultArr[]=[
        			'voter_card_no' => $voter->voter_card_no, 
        			'constituency_type_id' => $voter->getconstituency->id, 
        			'constituency_value' => $voter->constituency_value, 
        			'sr_no' => $voter->sr_no, 
        			'photo' => $voter->photo, 
        			'voter_name' => $voter->voter_name, 
        			'mobile_no' => $voter->mobile_no, 
        			'relation_name' => $voter->relation_name, 
        			'surname' => $voter->surname, 
        			'relation_type' => $voter->relation_type, 
        			'house_no' => $voter->house_no, 
        			'epic_id' => $voter->epic_id, 
        			'address' => $voter->address, 
        			'village_name' => $voter->village_name, 
        			'booth_address' => $voter->booth_address, 
        			'society' => $voter->society, 
        			'bibhag' => $voter->bibhag, 
        			'old_town' => $voter->old_town, 
        			'caste' => $voter->caste, 
        			'support' => $voter->support, 
        			'blood_group' => $voter->blood_group, 
        			'education' => $voter->education, 
        			'occupation' => $voter->occupation, 
        			'birth_date' => $voter->birth_date, 
        			'marriage_date' => $voter->marriage_date, 
        			'email' => $voter->email, 
        			'aadhar_card' => $voter->aadhar_card, 
        			'ration_card' => $voter->ration_card, 
        			'is_dead' => $voter->is_dead, 
        			'gender' => $voter->gender, 
        			'is_head_of_family' => $voter->is_head_of_family
            	];
            }
        }else{
            $allVoter = Votercondidateupdate::select('voter_card_no', 'constituency_type_id', 'constituency_value', 'sr_no', 'photo', 'voter_name', 'mobile_no', 'relation_name', 'surname', 'relation_type', 'house_no', 'epic_id', 'address', 'village_name', 'booth_address', 'society', 'bibhag', 'old_town', 'caste', 'support', 'blood_group', 'education', 'occupation', 'birth_date', 'marriage_date', 'email', 'aadhar_card', 'ration_card', 'is_dead', 'gender', 'is_head_of_family')->get();
            foreach ($allVoter as $key => $voter) {
                $resultArr[]=[
                    'voter_card_no' => $voter->voter_card_no, 
                    'constituency_type_id' => $voter->getconstituency->id, 
                    'constituency_value' => $voter->constituency_value, 
                    'sr_no' => $voter->sr_no, 
                    'photo' => $voter->photo, 
                    'voter_name' => $voter->voter_name, 
                    'mobile_no' => $voter->mobile_no, 
                    'relation_name' => $voter->relation_name, 
                    'surname' => $voter->surname, 
                    'relation_type' => $voter->relation_type, 
                    'house_no' => $voter->house_no, 
                    'epic_id' => $voter->epic_id, 
                    'address' => $voter->address, 
                    'village_name' => $voter->village_name, 
                    'booth_address' => $voter->booth_address, 
                    'society' => $voter->society, 
                    'bibhag' => $voter->bibhag, 
                    'old_town' => $voter->old_town, 
                    'caste' => $voter->caste, 
                    'support' => $voter->support, 
                    'blood_group' => $voter->blood_group, 
                    'education' => $voter->education, 
                    'occupation' => $voter->occupation, 
                    'birth_date' => $voter->birth_date, 
                    'marriage_date' => $voter->marriage_date, 
                    'email' => $voter->email, 
                    'aadhar_card' => $voter->aadhar_card, 
                    'ration_card' => $voter->ration_card, 
                    'is_dead' => $voter->is_dead, 
                    'gender' => $voter->gender, 
                    'is_head_of_family' => $voter->is_head_of_family
                ];
            }
        }
        return collect($resultArr);
    }


    public function headings(): array
    {
        return ['voter_card_no', 'constituency_type_id', 'constituency_value', 'sr_no', 'photo', 'voter_name', 'mobile_no', 'relation_name', 'surname', 'relation_type', 'house_no', 'epic_id', 'address', 'village_name', 'booth_address', 'society', 'bibhag', 'old_town', 'caste', 'support', 'blood_group', 'education', 'occupation', 'birth_date', 'marriage_date', 'email', 'aadhar_card', 'ration_card', 'is_dead', 'gender', 'is_head_of_family'
        ];
    }
}
