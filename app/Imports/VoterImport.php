<?php

namespace App\Imports;

use App\Voter;
use App\Votercondidateupdate;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Session;

#use App\Constituencytype;

class VoterImport implements ToCollection
{

    public function collection(Collection $rows)
    {
        $constituency_value=Session::get('constituency_value');
        $constituency_id=Session::get('constituency_id');
        $candidate_id=Session::get('candidate_id');

        foreach ($rows as $key=>$row) {
            if($key>0){

                if($candidate_id==0){
                    $existingVoter=Voter::where('voter_card_no', $row[0])->where('constituency_type_id', $constituency_id);
                }else{
                    $existingVotercandidateupdate=Votercondidateupdate::where('voter_card_no', $row[0])->where('constituency_type_id', $constituency_id)->where('candidate_id', $candidate_id);

                    $voterData=Voter::where('voter_card_no', $row[0])->where('constituency_type_id', $constituency_id);
                }
                if($existingVoter->count()==0){
                    if($candidate_id==0){
                        Voter::create([
                            'voter_card_no'        => $row[0],
                            'constituency_type_id' => $row[1],
                            'constituency_value'   => $row[2],
                            'sr_no'                => $row[3],
                            'photo'                => $row[4],
                            'voter_name'           => $row[5],
                            'mobile_no'            => $row[6],
                            'relation_name'        => $row[7],
                            'surname'              => $row[8],
                            'relation_type'        => $row[9],
                            'house_no'             => $row[10],
                            'epic_id'              => $row[11],
                            'address'              => $row[12],
                            'village_name'         => $row[13],
                            'booth_address'        => $row[14],
                            'society'              => $row[15],
                            'bibhag'               => $row[16],
                            'old_town'             => $row[17],
                            'caste'                => $row[18],
                            'support'              => $row[19],
                            'blood_group'          => $row[20],
                            'education'            => $row[21],
                            'occupation'           => $row[22],
                            'birth_date'           => $row[23],
                            'marriage_date'        => $row[24],
                            'email'                => $row[25],
                            'aadhar_card'          => $row[26],
                            'ration_card'          => $row[27],
                            'is_dead'              => $row[28],
                            'gender'               => $row[29],
                            'is_head_of_family'    => $row[30],
                        ]);
                    }else{
                        Votercondidateupdate::create([
                            'candidate_id'=>$candidate_id, 
                            'voter_id'=>$voterData->get('id')->first()->id, 
                            'voter_card_no'        => $row[0],
                            'constituency_type_id' => $row[1],
                            'constituency_value'   => $row[2],
                            'sr_no'                => $row[3],
                            'photo'                => $row[4],
                            'voter_name'           => $row[5],
                            'mobile_no'            => $row[6],
                            'relation_name'        => $row[7],
                            'surname'              => $row[8],
                            'relation_type'        => $row[9],
                            'house_no'             => $row[10],
                            'epic_id'              => $row[11],
                            'address'              => $row[12],
                            'village_name'         => $row[13],
                            'booth_address'        => $row[14],
                            'society'              => $row[15],
                            'bibhag'               => $row[16],
                            'old_town'             => $row[17],
                            'caste'                => $row[18],
                            'support'              => $row[19],
                            'blood_group'          => $row[20],
                            'education'            => $row[21],
                            'occupation'           => $row[22],
                            'birth_date'           => $row[23],
                            'marriage_date'        => $row[24],
                            'email'                => $row[25],
                            'aadhar_card'          => $row[26],
                            'ration_card'          => $row[27],
                            'is_dead'              => $row[28],
                            'gender'               => $row[29],
                            'is_head_of_family'    => $row[30],
                        ]);
                    }
                }else{
                    if($candidate_id==0){
                        $existingVoter->update([
                            'voter_card_no'        => $row[0],
                            'constituency_type_id' => $row[1],
                            'constituency_value'   => $row[2],
                            'sr_no'                => $row[3],
                            'photo'                => $row[4],
                            'voter_name'           => $row[5],
                            'mobile_no'            => $row[6],
                            'relation_name'        => $row[7],
                            'surname'              => $row[8],
                            'relation_type'        => $row[9],
                            'house_no'             => $row[10],
                            'epic_id'              => $row[11],
                            'address'              => $row[12],
                            'village_name'         => $row[13],
                            'booth_address'        => $row[14],
                            'society'              => $row[15],
                            'bibhag'               => $row[16],
                            'old_town'             => $row[17],
                            'caste'                => $row[18],
                            'support'              => $row[19],
                            'blood_group'          => $row[20],
                            'education'            => $row[21],
                            'occupation'           => $row[22],
                            'birth_date'           => $row[23],
                            'marriage_date'        => $row[24],
                            'email'                => $row[25],
                            'aadhar_card'          => $row[26],
                            'ration_card'          => $row[27],
                            'is_dead'              => $row[28],
                            'gender'               => $row[29],
                            'is_head_of_family'    => $row[30],
                        ]);
                    }else{
                        $existingVotercandidateupdate->update([
                            'candidate_id'=>$candidate_id, 
                            'voter_id'=>$voterData->get('id')->first()->id, 
                            'voter_card_no'        => $row[0],
                            'constituency_type_id' => $row[1],
                            'constituency_value'   => $row[2],
                            'sr_no'                => $row[3],
                            'photo'                => $row[4],
                            'voter_name'           => $row[5],
                            'mobile_no'            => $row[6],
                            'relation_name'        => $row[7],
                            'surname'              => $row[8],
                            'relation_type'        => $row[9],
                            'house_no'             => $row[10],
                            'epic_id'              => $row[11],
                            'address'              => $row[12],
                            'village_name'         => $row[13],
                            'booth_address'        => $row[14],
                            'society'              => $row[15],
                            'bibhag'               => $row[16],
                            'old_town'             => $row[17],
                            'caste'                => $row[18],
                            'support'              => $row[19],
                            'blood_group'          => $row[20],
                            'education'            => $row[21],
                            'occupation'           => $row[22],
                            'birth_date'           => $row[23],
                            'marriage_date'        => $row[24],
                            'email'                => $row[25],
                            'aadhar_card'          => $row[26],
                            'ration_card'          => $row[27],
                            'is_dead'              => $row[28],
                            'gender'               => $row[29],
                            'is_head_of_family'    => $row[30],
                        ]);
                    }
                }
                
            }
        }
    }

    public function headingRow(): int
    {
        return 1;
    }
}
