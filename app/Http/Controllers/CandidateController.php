<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Candidate;
use App\Constituency;
use Carbon\Carbon;
use Validator;
use File;

class CandidateController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $allCandidate= Candidate::orderBy('updated_at', 'desc')->get()->chunk(6);
        #dd($allCandidate);
        $data = [
            'allCandidate'=>$allCandidate
        ];
        return view('manageCandidate.index', compact('data'));

    }

    public function single($record_id = 0)
    {
        $candidate = Candidate::find($record_id);
        $allConstituency= Constituency::pluck('name','id');
        $data = [
            'candidate'=>$candidate,
            'allConstituency'=>$allConstituency
        ];
        return view('manageCandidate.single', compact('data'));
    }
    public function save(Request $request)
    {
        if (!empty($request->all())) {
            #dd($request->all());
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'constituency_id' => 'required',
                'username' => 'required',
                'mobile_no' => 'required',
                'email' => 'required',
                'password' => 'required',
                'confirm_password' => 'required|same:password',
                'photo' => 'nullable|mimes:jpeg,jpg,png',
                'logo' => 'nullable|mimes:jpeg,jpg,png',
            ]);

            if($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            }else {
                //try {
                
                    if ($request->record_id > 0) {
                        $record = Candidate::find($request->record_id);

                        $record->update([
                            'name'=> $request->name, 
                            'constituency_id'=> $request->constituency_id, 
                            'username'=> $request->username, 
                            //'photo'=> $request->photo, 
                            'mobile_no'=> $request->mobile_no, 
                            'email'=> $request->email, 
                            'password'=> $request->password, 
                            //'logo'=> $request->logo
                        ]);

                        if(!empty($request->photo)){
                            if ($record) {
                                File::delete(storage_path('app/public/candidates/photo/' . $record->first()->photo));
                            }
                        }
                        if(!empty($request->photo)){
                            if ($record) {
                                File::delete(storage_path('app/public/candidates/logo/' . $record->first()->logo));
                            }
                        }
                    } else {
                        $record = Candidate::create([
                            'name'=> $request->name, 
                            'constituency_id'=> $request->constituency_id, 
                            'username'=> $request->username, 
                            //'photo'=> $request->photo, 
                            'mobile_no'=> $request->mobile_no, 
                            'email'=> $request->email, 
                            'password'=> $request->password, 
                            //'logo'=> $request->logo
                        ]);
                    }
                    if(!empty($request->photo)){
                        $file     = $request->file('photo');
                        $fileName = md5(Carbon::now()) . $file->getClientOriginalName();
                        $fileName = str_replace(' ', '-', strtolower(trim($fileName)));
                        $file->move(storage_path('app/public/candidates/photo/'), $fileName);

                        $record->update(['photo' => $fileName]);
                    }
                    if(!empty($request->logo)){
                        $file     = $request->file('logo');
                        $fileName = md5(Carbon::now()) . $file->getClientOriginalName();
                        $fileName = str_replace(' ', '-', strtolower(trim($fileName)));
                        $file->move(storage_path('app/public/candidates/logo/'), $fileName);

                        $record->update(['logo' => $fileName]);
                    }
                    return redirect()->route('home');

                /*} catch (\Exception $exc) {
                    
                    return redirect()->route('home');
                }*/
            }
        } 
        //return redirect()->route('home');
    }
}
