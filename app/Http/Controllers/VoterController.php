<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\VoterExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Candidate;
use App\Constituency;
use App\Imports\VoterImport;
use Session;

class VoterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function export(Request $request) 
    {
        Session::put('candidate_id',$request->candidate_id);
        Session::put('constituency_id',$request->constituency_id);
        Session::put('constituency_value',$request->constituency_value);
        return Excel::download(new VoterExport, 'voters.xlsx');
    }

    public function import(Request $request) 
    {

        Session::put('candidate_id',$request->candidate_id);
        Session::put('constituency_id',$request->constituency_id);
        Session::put('constituency_value',$request->constituency_value);
        Excel::import(new VoterImport, request()->file('excel_file'));
        
        return redirect('/voter-import')->with('success', 'All good!');
    }
    public function importForm()
    {
        
        $allCandidate= Candidate::select('name','id','constituency_id')->get();
        $allConstituency= Constituency::pluck('name','id');
        #dd($allCandidate);
        $data = [
            'allCandidate'=>$allCandidate,
            'allConstituency'=>$allConstituency
        ];
        return view('manageVoter.import', compact('data'));
    }
    public function exportForm()
    {
        $allCandidate= Candidate::select('name','id','constituency_id')->get();
        $allConstituency= Constituency::pluck('name','id');
        #dd($allCandidate);
        $data = [
            'allCandidate'=>$allCandidate,
            'allConstituency'=>$allConstituency
        ];
        return view('manageVoter.export', compact('data'));
    }
}
