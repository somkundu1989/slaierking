<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Constituency extends Model
{
    use SoftDeletes;

    protected $table = 'constituency_master';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id', 'name', 'constituency_type_id', 'state_id', 'created_at', 'updated_at'
    ];

    public function constituencytype()
    {
        return $this->belongsTo('App\Constituencytype', 'constituency_type_id');
    }
    public function state()
    {
        return $this->belongsTo('App\State', 'state_id');
    }
}
