<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voter extends Model
{
    use SoftDeletes;

    protected $table = 'voter_master';

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'voter_card_no', 'constituency_type_id', 'constituency_value', 'sr_no', 'photo', 'voter_name', 'mobile_no', 'relation_name', 'surname', 'relation_type', 'house_no', 'epic_id', 'address', 'village_name', 'booth_address', 'society', 'bibhag', 'old_town', 'caste', 'support', 'blood_group', 'education', 'occupation', 'birth_date', 'marriage_date', 'email', 'aadhar_card', 'ration_card', 'is_dead', 'gender', 'is_head_of_family', 'gender_en', 'age_en', 'created_at', 'updated_at'
	];

    public function getAllUpdatedVoter()
    {
        return $this->hasMany('App\Votercondidateupdate', 'voter_id');
    }
    public function getConstituency()
    {
        return $this->belongsTo('App\Constituencytype', 'constituency_type_id');
    }
}
